package dodopriester.World;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import dodopriester.Block.Block;
import dodopriester.Block.Dirt;
import dodopriester.Item.ItemSprite;
import dodopriester.Main.Board;

public class World implements WorldManager {
	
	private String name;
	private int id;
	
	public static final int MAX_WORLD_X = 5120;
	public static final int MAX_WORLD_Y = 3000;
	
	public static final int VIEWPORT_SIZE_X = 1920;
	public static final int VIEWPORT_SIZE_Y = 1820;
	
	public static final int offsetMaxX = MAX_WORLD_X - VIEWPORT_SIZE_X;
	public static final int offsetMaxY = MAX_WORLD_Y - VIEWPORT_SIZE_Y;
	public static final int offsetMinX = 0;
	public static final int offsetMinY = 0;
	
	public World() {
	name = "World";
	id = 0;
	
	generateWorld(10, 2);
	
	}
	
	//The items List collects all current items in this world.
	public ArrayList<ItemSprite> items = new ArrayList<>();
	
	//The blocks Array lists all current blocks in this world.
	public ArrayList<Block> blocks = new ArrayList<>();
	
	@Override
	public void generateWorld(int maxLvl, int maxDiff) {
	//Run procedure until block.
		long startTime = System.nanoTime();
		
		for(int i2 = 0; i2 < maxLvl; i2++) {
		int i3 = MAX_WORLD_Y-720-i2*40;
		
		for(int i = 0; i < MAX_WORLD_X; i = i + 40) {
			addBlock(new Dirt(i, i3));
			
			int i4 = ThreadLocalRandom.current().nextInt(0, maxDiff);
			
			if(!(i4 == 0)) {
			addBlock(new Dirt(i, i3-(i4*40)));
			
				if(i4 > 1) {
					for(int i5 = 0; i5 < i4+maxDiff; i5++) {
						addBlock(new Dirt(i, i3-(i5*40)));
					}
				}
			}
		}
		
	}
		System.out.println((System.nanoTime() - startTime)/1000000 + "ms passed for generating Blocks!");
	}
	
	// Check all Blocks if its overlapping the x and y values.
		public boolean isWorldBlockOverlapping(int x, int y) {
		int size = Board.world.blocks.size();
		
		for(int i = 0; i < size; i++) {
		Block b = Board.world.blocks.get(i);
		
		Rectangle r = b.getBounds();
		Rectangle r2 = new Rectangle(x, y, 2, 2);
			
		if(r2.intersects(r)) {
			return true;
		}
		}
		return false;
	}
		
	@Override
	public World getWorld() {
		return this;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public int getGravity() {
		return GRAVITY;
	}

	public void addItem(ItemSprite item) {
	items.add(item);
	}
	
	public void addBlock(Block block) {
		blocks.add(block);
	}
	
	public void removeItem(int index) {
		items.remove(index);
	}
	
	public void removeBlock(int index) {
		blocks.remove(index);
	}
	
}
