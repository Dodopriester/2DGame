package dodopriester.World;

public interface WorldManager {

	/* The WorldManager interface is used to create a new World.
	 * Usage: [Modifiers] [Class Name] implements WorldManager
	 */
	
	// NOTE: The velocity will be subtracted by the gravity divided by 4
	public static final int GRAVITY = 20;
	
	public String getName();
	
	public void generateWorld(int maxLvl, int maxDiff);
	
	public int getID();
	
	public int getGravity();
	
	public World getWorld();
	
}
