package dodopriester.Block;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import dodopriester.Sprite.Sprite;

public abstract class Block extends Sprite {

	/* To create a new Block, create a class, set the superclass to a Block class
	 * Constructor Example:
	 * public [Class Name](int x, int y) {
	 * this.x = x;
	 * this.y = y;
	 * 
	 * setSprite(Image img); 
	 * }
	 */
	
	public int solidLvl;
	
	protected int x;
	protected int y;
	
	public Block(int solidLVL, int x, int y) {
	
	try {
		sprite = ImageIO.read(new File("assets/dodopriester/images/sprites/block/testBlock.png"));
	} catch (IOException e) {
		e.printStackTrace();
	}
	solidLvl = solidLVL;
	
	this.x = x;
	this.y = y;
	}
	
	protected void setSprite(Image img) {
		sprite = img;
	}
	
	
}
