package dodopriester.Block;

import java.awt.Rectangle;

public class Dirt extends Block {
	
	private int width = 40;
	private int height = 40;
	
	public Dirt(int x, int y) {
		super(1, x, y);
		
		this.x = x;
		this.y = y;
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);
	}

	@Override
	public int getSpriteX() {
		return x;
	}

	@Override
	public int getSpriteY() {
		return y;
	}

}
