package dodopriester.Sprite;

import java.awt.Image;
import java.awt.Rectangle;

import dodopriester.Block.Block;
import dodopriester.Main.Board;

public abstract class Sprite implements SpriteListener {

	public double velocity = 0;
	public double weight = 25;
	
	public int width = 10;
	public int height = 20;
	
	public int locX;
	public int locY;
	
	public static Sprite instance;
	
	protected Image sprite;
	
	public boolean hasGravity = false;
	
	public Sprite() {
		Board.sprites.add(instance);
	}
	
	@Override
	public void updateGravity() {
		if(hasGravity) {
			if(velocity > -Board.world.getGravity()*2) {
				velocity = velocity - Board.world.getGravity()/4;
			}
		}
	}
	
	@Override
	// Check all Blocks if its overlapping the x and y values.
	public boolean isBlockOverlapping(int x, int y) {
		int size = Board.world.blocks.size();
		
		for(int i = 0; i < size; i++) {
		Block b = Board.world.blocks.get(i);
		
		Rectangle r = b.getBounds();
		Rectangle r2 = new Rectangle(x, y, width, height);
		
		if(r2.intersects(r)) {
			return true;
		}
		}
		return false;
	}
	
	@Override
	public Image getSprite() {
		return sprite;
	}
	
}
