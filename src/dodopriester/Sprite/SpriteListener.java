package dodopriester.Sprite;

import java.awt.Image;
import java.awt.Rectangle;

public interface SpriteListener {

	public Rectangle getBounds();
	
	public int getSpriteX();
	
	public int getSpriteY();
	
	public Image getSprite();
	
	public void updateGravity();
	
	public boolean isBlockOverlapping(int x, int y);
	
}
