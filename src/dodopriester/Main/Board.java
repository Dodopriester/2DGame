package dodopriester.Main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.Timer;

import dodopriester.Block.Block;
import dodopriester.Entity.Player;
import dodopriester.Item.ItemSprite;
import dodopriester.Item.MasterItem;
import dodopriester.Sprite.Sprite;
import dodopriester.World.World;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

@SuppressWarnings("serial")
public class Board extends JPanel implements ActionListener,MouseListener {

	//The Board class is the main class for displaying.
	
	private Timer timer;
	
	private final int DELAY = 25;
	
	public Player p;
	
	public static World world;

	public static ArrayList<Sprite> sprites = new ArrayList<>();
	
	public Board() {
	
	initBoard();
	}

	private void initBoard() {
	p = new Player();
	
	timer = new Timer(DELAY, this);
	timer.start();
	
	world = new World();
	
	world.addItem(new ItemSprite(new MasterItem(), 500, 500));
	
	setBackground(Color.BLUE);
	
	}
	
	private void drawItem(Graphics g, ItemSprite item) {
		Graphics2D g2D = (Graphics2D) g;
		
		if(item.item.useSprite()) {
			g2D.drawImage(item.item.sprite, item.getSpriteX(), item.getSpriteY(), null);
		} else {
			g2D.drawImage(item.item.img, item.getSpriteX(), item.getSpriteY(), null);
		}
	}
	
	private void drawBlock(Graphics g, Block block) {
		Graphics2D g2D = (Graphics2D) g;
		
		g2D.drawImage(block.getSprite(), block.getSpriteX(), block.getSpriteY(), null);
	}
	
	public static void playSound(final String URL) throws IOException {
	    // open the sound file as a Java input stream
	    InputStream in = new FileInputStream(URL);

	    // create an audiostream from the inputstream
	    AudioStream audioStream = new AudioStream(in);

	    // play the audio clip with the audioplayer class
	    AudioPlayer.player.start(audioStream);
	}
	
	private void updatePlayerSprite(Graphics g) {
		p.setPlayerSprite();
		
		Graphics2D g2D = (Graphics2D) g;
		g2D.drawImage(p.getPlayerImage(), p.getSpriteX(), p.getSpriteY(), null);
		
	}
	
	private boolean checkItemCollision() {
	for(int i = 0; i < world.items.size(); i++) {
		ItemSprite item = world.items.get(i);
		
		Rectangle r = item.getBounds();
		
		if(p.getBounds().intersects(r)) {
			p.addInv(item.item);
			
			try {
				playSound("assets/dodopriester/sounds/Item.wav");
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			world.removeItem(i);
			return true;
		}
	}
	return false;
	}
	
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.translate(-p.camX, -p.camY);
		
		updatePlayerSprite(g);
		
		for(int i = 0; i < world.items.size(); i++) {
			drawItem(g, world.items.get(i));
		}
		
		for(int i = 0; i < world.blocks.size(); i++) {
			drawBlock(g, world.blocks.get(i));
		}
		
	}

	
	@Override
	public void actionPerformed(ActionEvent arg0) {
	checkItemCollision();
	p.updateGravity();
	
	p.camX = p.locX - World.VIEWPORT_SIZE_X / 2;
	p.camY = p.locY+320 - World.VIEWPORT_SIZE_Y / 2;
	
	if (p.camX > World.offsetMaxX) {
		p.camX = World.offsetMaxX;
	} else if (p.camX < World.offsetMinX) {
		p.camX = World.offsetMinX;
	}
	
	if (p.camY > World.offsetMaxY) {
		p.camY = World.offsetMaxY;
	} else if (p.camY < World.offsetMinY) {
		p.camY = World.offsetMinY;
	}
	
	repaint();
	
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		
		if(world.isWorldBlockOverlapping(x, y)) {
			for(int i = 0; i < world.blocks.size(); i++) {
				Block b = world.blocks.get(i);
				
				if(b.getBounds().intersects(new Rectangle(x, y, 1, 1))) {
					world.removeBlock(i);
				}
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}
	
}