package dodopriester.Main;

import java.awt.EventQueue;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;


@SuppressWarnings("serial")
public class Application extends JFrame {

	public Board b;
	
	public Application() {
	
	initUI();
	}

	//Set the default settings for the game.
	private void initUI() {
	b = new Board();
		
	add(b);
	addKeyListener(new TAdapter());
	addMouseListener(b);
	
	setSize(1920, 1820);
	
	setTitle("2DGame Example");
	setDefaultCloseOperation(EXIT_ON_CLOSE);
	setLocationRelativeTo(null);
	
	}
	
	public static void main(String[] args) {
//	long startTime = System.nanoTime();
	
	EventQueue.invokeLater(new Runnable() {
		
		@Override
		public void run() {
		Application ex = new Application();
		ex.setVisible(true);

		System.out.println("Startet Game");
		System.out.println(Board.sprites.size() + " Sprites created!");
//		System.out.println((System.nanoTime() - startTime)/1000000 + "ms passed!");
		}
	});
	
	}
	
	//If you press a key.
	private class TAdapter extends KeyAdapter {
		@Override
        public void keyReleased(KeyEvent e) {
            //p.keyReleased(e);
        }

        @Override
        public void keyPressed(KeyEvent e) {
            b.p.keyPressed(e);
        }
	}
	
}