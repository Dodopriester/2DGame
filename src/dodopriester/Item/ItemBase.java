package dodopriester.Item;

import java.awt.Image;

import javax.swing.ImageIcon;

public abstract class ItemBase {

	//Initialize the core values of an item
	public String name;
	public int id;
	public String desc;
	public Image img;
	public Image sprite;
	public int quantity;
	
	public ItemBase(String Name, int ID, String Desc, int Quantity) {
	name = Name;
	id = ID;
	desc = Desc;
	quantity = Quantity;
	
	initItem();
	}

	private void initItem() {
	ImageIcon i = new ImageIcon("assets/com/gmail/toadiboy/lp/Rise/images/testSprite.png");
	
	img = i.getImage();
	}
	
	public void setImage(Image i) {
		img = i;
	}
	
	public void setSprite(Image i) {
		sprite = i;
	}
	
	public boolean useSprite() {
		return sprite != null;
	}
	
	//Override this function to add it a use
	public void useItem() {
		System.out.println("You used a item!");
	}
	
	protected void setItem(String Name, int ID, String Desc, int Quantity) {
	name = Name;
	id = ID;
	desc = Desc;
	quantity = Quantity;
	}
}
