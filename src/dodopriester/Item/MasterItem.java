package dodopriester.Item;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class MasterItem extends ItemBase {

	/* If you want to create a new item, create a class
	 * and set extend to MasterItem. Then you call in the initializer the 
	 * setItem(Name, ID, Desc, Quantity) function.
	 * If you want to add a img/sprite, then call the setImage/setSprite function.
	 * You'll have to set at least an image, because the image will be showed
	 * in the inventory. If you want to set a separate sprite for the world, you can call
	 * the set sprite function. The game checks if you don't use a separate sprite, the normal image 
	 * will be showed in the world.
	 */
	
	public MasterItem() {
		super("MasterName", 0, "The basic item", 1);
		
		try {
			Image image = ImageIO.read(new File("assets/dodopriester/images/sprites/items/test.png"));
			setImage(image);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
