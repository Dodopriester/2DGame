package dodopriester.Item;

import java.awt.Rectangle;

import dodopriester.Sprite.Sprite;

public class ItemSprite extends Sprite {

	public MasterItem item;
	private int x;
	private int y;
	
	public ItemSprite(MasterItem item, int x, int y) {
	this.x = x;
	this.y = y;
	this.item = item;
	
	this.item.sprite = sprite;
	}
	
	public Rectangle getBounds() {
		if(item.useSprite()) {
			return new Rectangle(x, y, item.sprite.getWidth(null), item.sprite.getHeight(null));
		} else {
			return new Rectangle(x, y, item.img.getWidth(null), item.img.getHeight(null));
		}
	}
	
	@Override
	public int getSpriteX() {
		return x;
	}

	@Override
	public int getSpriteY() {
		return y;
	}

}
