package dodopriester.Entity;

import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import dodopriester.Item.MasterItem;
import dodopriester.Main.Board;

public class Player extends Entity {
	
	//Initialize player values 
	
	//The x and y variables are for the sprite, please use for the location the locX and locY variable.
	private int x = 0;
	private int y = 0;
	
	//The sprite variable shows the final result. The spriteImg gets the subImage from the sprite.
	private BufferedImage spriteImg;
	
	//The animate showSprite resets the image, if the updateSprite function is called.
	private boolean showSprite = true;
	
	public int velocity = 0;
	public int velocityX = 0;
	
	public int camX;
	public int camY;
	
	public ArrayList<MasterItem> inv = new ArrayList<>();
	
	public Player() {

	try {
		initPlayer();
	} catch (IOException e) {
		e.printStackTrace();
	}
	}

	
	private void initPlayer() throws MalformedURLException, IOException {
	
	hasGravity = true;
	
	locX = 600;
	locY = 20;
	
	width = 20;
	height = 30;
	
	//Load the player sprite.
	spriteImg = ImageIO.read(new File("assets/dodopriester/images/sprites/entity/PlayerSprite.png"));
	
	sprite = spriteImg.getSubimage(x, y, width, height);
	
	}
	
	public void setPlayerSprite() {
		sprite = spriteImg.getSubimage(x, y, width, height);
	}
	
	
	//Set the player physics
	@Override
	public void updateGravity() {

		//Checks if the Player's target location is overlapping, if true the velocity will be set to 0.
		if(!isBlockOverlapping(locX, locY - velocity) && !isBlockOverlapping(locX, locY - Board.world.getGravity()/4) ) {
		locY = locY - velocity;
		} else {
		velocity = 0;
		}
		
		if(!isBlockOverlapping(locX - velocityX, locY)) {
			locX = locX - velocityX;
		} else {
			velocityX = 0;
		}
		
		//If the velocity of the player is not under -20, it will be subtracted by the world gravity/16
		if(velocity > -10) {
			velocity = velocity - Board.world.getGravity()/4;
		}
		
		if(velocityX > 0) {
			velocityX--;
		} else if (velocityX < 0){
			velocityX++;
		}
		}
	
	public boolean isOnGround() {
		return isBlockOverlapping(locX, locY + 20);
	}
	
	//If key pressed
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		
		if(key == KeyEvent.VK_W) {
			if(!isBlockOverlapping(locX, locY - 10)) {
			if(isOnGround()) {
			y = 0;
			velocity = 40;
			
			showSprite = true;
			}
			}
		}
		
		if(key == KeyEvent.VK_A) {
		if(!isBlockOverlapping(locX - 10, locY)) {
			y = 30;
			
			if(velocityX < 10) {	
				velocityX = velocityX + 10;
			}
			
			showSprite = true;
		}
		}
		
		if(key == KeyEvent.VK_D) {
		if(!isBlockOverlapping(locX + 10, locY)) {
			y = 60;
			
			if(velocityX > -10) {
				velocityX = velocityX - 10;
			}
			
			
			showSprite = true;
		}
		}
		
		if(key == KeyEvent.VK_S) {
		if(!isBlockOverlapping(locX, locY + 10)) {
			y = 90;
			
			locY = locY + 10;
			
			showSprite = true;
		}
		}
	}
	
	//If key released
	/*public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		
		if(key == KeyEvent.VK_W) {
			y = 0;
			x = 0;
			
			try {
				updateSprite();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
		if(key == KeyEvent.VK_A) {
			y = 0;
			x = 0;
			
			try {
				updateSprite();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
		}
		
		if(key == KeyEvent.VK_S) {
			y = 0;
			x = 0;
			
			try {
				updateSprite();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

		}
		
		if(key == KeyEvent.VK_D) {
			y = 0;
			x = 0;
			
			try {
				updateSprite();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		
		}
	}
	*/
	
	
	
	public boolean isShowingSprite() {
		return showSprite;
	}
	
	public Image getPlayerImage() {
		return sprite;
	}


	@Override
	public Rectangle getBounds() {
		return new Rectangle(locX, locY, width, height);
	}


	@Override
	public int getSpriteX() {
		return locX;
	}
	
	@Override
	public int getSpriteY() {
		return locY;
	}
	
	public void addInv(MasterItem item) {
		inv.add(item);
	}
}
